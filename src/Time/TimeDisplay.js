import React, { useEffect, useState } from "react";

function TimeDisplay() {
  const [time, setTime] = useState(null);

  useEffect(() => {
    // Update time every second
    const interval = setInterval(() => {
      const currentTime = new Date();
      setTime(currentTime.toLocaleTimeString());
    }, 1000);

    // Clean up the interval on component unmount
    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <div style={{color: 'white'}}>
      {time ? `The current time is ${time}` : 'Loading...'}
    </div>
  );
}

export default TimeDisplay;
